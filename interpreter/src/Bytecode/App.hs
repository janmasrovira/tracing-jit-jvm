module Bytecode.App (main) where

import System.Environment
import Bytecode.Parser
import Control.Monad.Except
import Bytecode.Interpreter

main :: IO ()
main = do
  [path] <- getArgs
  bytecodestr <- readFile path
  case runExcept (parse bytecodestr) of
    Left e -> error (show e)
    Right p -> runIO p
