module Bytecode.Parser (
  parse
  , module Bytecode.Grammar
  ) where

import Bytecode.Grammar.ErrM
import Bytecode.Grammar
import Bytecode.Grammar.ParBytecode
import Control.Monad.Except

-- | Returns the AST of a Bytecode program or a parser error.
parse :: String -> Except String Program
parse str = case pProgram (myLexer str) of
  Bad e -> throwError e
  Ok r  -> return r
