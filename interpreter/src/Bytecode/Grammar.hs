module Bytecode.Grammar
  (module Bytecode.Grammar.AbsBytecode
  , module Bytecode.Grammar.PrintBytecode
  , fromSInteger
  -- * Lenses
  , labelInt
  , lineLabel
  , lineInstr
  , localIxInt
  , methodSig
  , methodCode
  ) where

import           Bytecode.Grammar.AbsBytecode
import           Bytecode.Grammar.PrintBytecode
import           Lens.Micro.Platform
import           Text.PrettyPrint.HughesPJClass (Doc, Pretty, hcat, pPrint,
                                                 pPrintList, text, vcat, (<+>))
import qualified Text.PrettyPrint.HughesPJClass as PP


instance Num LocalIx where
  (LocalIx a) + (LocalIx b) = LocalIx (a + b)
  (LocalIx a) * (LocalIx b) = LocalIx (a * b)
  (LocalIx a) - (LocalIx b) = LocalIx (a - b)
  abs (LocalIx a) = LocalIx (abs a)
  signum (LocalIx a) = LocalIx (signum a)
  fromInteger = LocalIx

fromSInteger :: SInteger -> Integer
fromSInteger s = case s of
    PosInteger n -> n
    NegInteger n -> -n

instance Num SInteger where
  a + b = fromInteger (fromSInteger a + fromSInteger b)
  a - b = fromInteger (fromSInteger a - fromSInteger b)
  a * b = fromInteger (fromSInteger a * fromSInteger b)
  abs a = fromInteger (abs (fromSInteger a))
  signum s = fromInteger (signum (fromSInteger s))
  fromInteger n
    | n < 0 = NegInteger (abs n)
    | otherwise = PosInteger n

instance Pretty Label where
  pPrint (Label l) = PP.int (fromInteger l)

instance Pretty Instruction where
  pPrint = PP.text . render . prt 0

instance Pretty Line where
  pPrint = PP.text . render . prt 0

labelInt :: Lens' Label Int
labelInt = lens getter setter
  where getter (Label l) = fromInteger l
        setter _ = Label . toInteger

lineLabel :: Lens' Line Label
lineLabel = lens getter setter
  where getter (Line lbl _) = lbl
        setter (Line _ i) lbl = Line lbl i

lineInstr :: Lens' Line Instruction
lineInstr = lens getter setter
  where getter (Line _ i) = i
        setter (Line lbl _) = Line lbl

localIxInt :: Lens' LocalIx Int
localIxInt = lens getter setter
  where getter (LocalIx i) = fromInteger i
        setter _ = LocalIx . toInteger

methodSig ::  Lens' Method (Ident, [Type], Type)
methodSig = lens getter setter
  where getter (Method r name args _) = (name, args, r)
        setter (Method _ _ _ ls) (name, args, ret) = Method ret name args ls

methodCode ::  Lens' Method [Line]
methodCode = lens getter setter
  where getter (Method _ _ _ ls) = ls
        setter (Method a b c _) ls = Method a b c ls
