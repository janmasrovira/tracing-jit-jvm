{-# LANGUAGE DuplicateRecordFields      #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiWayIf                 #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TupleSections              #-}
{-# LANGUAGE ViewPatterns               #-}
module Bytecode.Interpreter (
  runIO
  , runPure
  ) where

import           Bytecode.Grammar
import           Control.Monad.Except
import           Control.Monad.Extra
import           Control.Monad.Primitive
import           Control.Monad.Reader
import           Control.Monad.ST
import           Control.Monad.State
import           Control.Monad.Writer
import           Data.Function
import           Data.IntMap                    (IntMap)
import qualified Data.IntMap                    as IMap
import           Data.IntSet                    (IntSet)
import qualified Data.IntSet                    as ISet
import           Data.List
import           Data.List.Extra
import           Data.List.NonEmpty             (NonEmpty (..))
import qualified Data.List.NonEmpty             as L1
import           Data.Map                       (Map)
import qualified Data.Map                       as Map
import           Data.Maybe
import           Data.Set                       (Set)
import qualified Data.Set                       as Set
import           Data.Vector                    (Vector)
import qualified Data.Vector                    as Vec
import           Data.Vector.Mutable            (MVector)
import qualified Data.Vector.Mutable            as MVec
import qualified Debug.Trace                    as T
import           Extra
import           Lens.Micro.Platform            hiding (ix)
import           Text.PrettyPrint.HughesPJClass (Doc, Pretty, hcat, pPrint,
                                                 pPrintList, text, vcat, ($$),
                                                 (<+>))
import qualified Text.PrettyPrint.HughesPJClass as PP
import           Text.Printf

newtype CodeId = CodeId {
  _codeIdInt :: Int
  }
  deriving (Show, Num, Ord, Eq, Enum, Integral, Real, Bounded)
makeLenses ''CodeId

data CompiledCode = CompiledCode {
  _entryLabel      :: Label
  , _compiledLines :: [Line]
  }
  deriving (Show, Eq, Ord)
makeLenses ''CompiledCode

data Edge =
  Seq (Maybe Label)
  | Branch Label (Maybe Label)
  | Invoke CodeId (Maybe Label)
  deriving (Show)

instance Pretty Edge where
  pPrint = text . show

edgeNextLabel :: Lens' Edge (Maybe Label)
edgeNextLabel = lens getter setter
  where getter e = case e of
          Seq n      -> n
          Branch _ n -> n
          Invoke _ n -> n
        setter e n = case e of
          Seq _      -> Seq n
          Branch l _ -> Branch l n
          Invoke c _ -> Invoke c n

data MValue p = MInt Int
  | MArrayRef Type Int (MVector p Int)
instance Show (MValue p) where
  show (MInt num) = "MInt " ++ show num
  show _          = "ArrayRef"

data Value = VInt Int
  | ArrayRef Type Int (Vector Int)
  deriving (Show)

instance Pretty Value where
  pPrint v = case v of
    VInt i           -> PP.int i
    ArrayRef _ _ vec -> text (show vec)

newtype LineId = LineId {
  _lineId :: (CodeId, Label)
  }
  deriving (Show, Eq, Ord)
makeLenses ''LineId

data TraceItem = TraceItem {
  _itemLineId  :: LineId
  , _callDepth :: Int
  } deriving (Show, Eq)
makeLenses ''TraceItem

lineIdLabel :: Lens' LineId Label
lineIdLabel = lineId . _2

lineIdCodeId :: Lens' LineId CodeId
lineIdCodeId = lineId . _1

lineIdInts :: Lens' LineId (Int, Int)
lineIdInts = lens getter setter
  where getter l = (l ^. l1, l ^. l2)
        setter ln (c, l) = set l2 l (set l1 c ln)
        l1, l2 :: Lens' LineId Int
        l1 = lineIdCodeId . codeIdInt
        l2 = lineIdLabel . labelInt

instance Pretty LineId where
  pPrint lid =
    PP.parens (PP.int cid <> text ", " <> PP.int lbl)
    where (cid, lbl) = lid ^. lineIdInts

data Tracing = Tracing {
  _heatMap  :: Map LineId Double
  , _trace  :: Maybe [TraceItem]
  , _active :: Bool
  }
  deriving (Show)
makeLenses ''Tracing

data LineStatistics = LStats {
  _interpretedRuns :: Int
  , _compiledRuns  :: Int
  , _wasCompiled   :: Bool
  }
  deriving (Show, Eq)
makeLenses ''LineStatistics

emptyLineStatistics :: LineStatistics
emptyLineStatistics = LStats {
  _interpretedRuns = 0
  , _compiledRuns = 0
  , _wasCompiled = False
  }

data Statistics = Stats {
  _count                :: Map LineId LineStatistics -- (Interpreted runs, Compiled runs)
  , _scompiledMethods   :: IntSet
  , _ffiCalls           :: Int
  , _compiledLinesCount :: Int
  }
  deriving (Show)
makeLenses ''Statistics

countLensAux :: Lens' LineStatistics a -> LineId -> Lens' Statistics a
countLensAux auxlens lid = count . at lid . non emptyLineStatistics . auxlens

countInterpreted :: LineId -> Lens' Statistics Int
countInterpreted = countLensAux interpretedRuns

countCompiled :: LineId -> Lens' Statistics Int
countCompiled = countLensAux compiledRuns

lineWasCompiled :: LineId -> Lens' Statistics Bool
lineWasCompiled = countLensAux wasCompiled

data MethodCode = MethodCode {
  _codeIden :: CodeId
  , _code   :: IntMap (NonEmpty Line)
} deriving (Show, Eq, Ord)
makeLenses ''MethodCode

newtype CompiledMethod = CMethod MethodCode
  deriving (Show)


-- | State of the interpreter.
data IState p = IState {
  _stack             :: [MValue p]
  , _locals          :: IntMap (MValue p)
  , _tracing         :: Tracing
  , _stats           :: Statistics
  , _compiledLoops   :: Map LineId CompiledCode
  , _compiledMethods :: IntMap CompiledMethod
} deriving (Show)
makeLenses ''IState

instance Pretty Statistics where
  pPrint Stats{..} =
    vcat
    [ text "Count: " <> PP.braces (vcat
      [  text (if b then "c " else "  ") <+> (pPrint ln <> PP.colon) <+> PP.int i <+> PP.int c
      | (ln, lstats) <- Map.assocs _count
      , let c = lstats ^. compiledRuns
            i = lstats ^. interpretedRuns
            b = lstats ^. wasCompiled])
    , text "Fully compiled methods:" <+> text (show (ISet.toList _scompiledMethods))
    , text (printf "Compiled instructions: %d/%d (%.2f%%)" compInstr totalInstr compInstrPerc)
    , text (printf "Total interpreted and compiled runs count: %d (%.2f%%) %d (%.2f%%)"
            runsInterp runsInterpPerc runsComp runsCompPerc)
    , text "Compiled lines: " <> PP.int _compiledLinesCount
    , text "FFI calls: " <> PP.int _ffiCalls
    ]
    where
      totalInstr = Map.size _count
      compInstr = Map.size (Map.filter (^. wasCompiled) _count)
      compInstrPerc, runsInterpPerc, runsCompPerc :: Double
      compInstrPerc = fromIntegral compInstr / fromIntegral totalInstr * 100
      runsInterp = sum (fmap (^. interpretedRuns) _count)
      runsComp = sum (fmap (^. compiledRuns) _count)
      runsTotal = runsInterp + runsComp
      runsInterpPerc = fromIntegral runsInterp / fromIntegral runsTotal * 100
      runsCompPerc = fromIntegral runsComp / fromIntegral runsTotal * 100

data LogMsg =
  LogDebug String
  | LogCompile LineId [Line]
  | LogCompiled CompiledCode
  | LogCompileMethod MethodCode
  | LogExitLoop Line
  | LogEnterLoop Line
  | LogJump Label
  deriving (Show, Eq, Ord)

instance Pretty CompiledCode where
  pPrint code = text "Entry:" <+> pPrint elbl $$ ppLines clines
    where
      ppLines = vcat . map pPrint
      elbl = code ^. entryLabel
      clines = code ^. compiledLines

instance Pretty LogMsg where
  pPrint msg = case msg of
    LogDebug str -> text "Message:" <+> text str
    LogCompile lid ls -> text "Compile hot loop triggered by" <+> pPrint lid $$ ppLines ls
    LogExitLoop ln -> text "Exit loop to" <+> text "run" <+> ppLine ln
    LogEnterLoop ln -> text "Enter compiled loop at" <+> ppLine ln
    LogCompileMethod m -> text "Compile method" <+> text (show m)
    LogJump lbl -> text "Jump to another compiled loop at" <+> text (show lbl)
    LogCompiled code -> text "Compiled code:" $$ pPrint code
    where ppLine = PP.pPrint
          ppLines = vcat . map ppLine

newtype Log = Log [LogMsg]
  deriving (Monoid, Show)

newtype CompactLog = CLog [(Int, [LogMsg])]

dropEqRpt :: Eq a => [a] -> [a] -> (Int, [a])
dropEqRpt x = go 0
  where
    go k ls
      | (hs, ts) <- splitAt n ls
      , hs == x = go (k + 1) ts
      | otherwise = (k, ls)
    n = length x

-- | The hack of all hacks...
-- Greedy algorithm that sometimes works
compressLog :: Log -> CompactLog
compressLog (Log ms) = CLog (go ms)
  where
    compressJumps :: [LogMsg] -> Maybe ((Int, [LogMsg]), [LogMsg])
    compressJumps [] = Nothing
    compressJumps [_] = Nothing
    compressJumps ms
      | Just ls@(h:hs) <- mls = let
          cycle = h : takeWhile (/=h) hs
          in
            case dropEqRpt cycle ls of
              (1, _)    -> Nothing
              (n, rest) -> Just ((n, map LogJump cycle), map LogJump rest)
      | otherwise = Nothing
      where
        mls :: Maybe [Label]
        mls = mapM isJump ms
        isJump (LogJump l) = Just l
        isJump _           = Nothing
    spanNotExit = span (not . isExit)
      where isExit LogExitLoop{} = True
            isExit _             = False
    go :: [LogMsg] -> [(Int, [LogMsg])]
    go [] = []
    go (l@LogEnterLoop{}:ls0) =
      let (js, (ex:ls1)) = spanNotExit ls0
          cycle = l : js ++ [ex]
          n = length cycle
          (rpts, ls2) = dropEqRpt cycle ls1
      in case compressJumps js of
        Just ((n, jcycle), rest) -> (1, [l]) : (n, jcycle) : go (rest ++ (ex:ls1))
        Nothing -> (1 + rpts, cycle) : go ls2
    go (l:ls) = (1, [l]) : go ls


instance Pretty CompactLog where
  pPrint (CLog ls) = vsep (map pline ls)
    where pline (1, ls) = vcat (map pPrint ls)
          pline (k, ls) = text "Repeat" <+> PP.int k <+> text "times: " $$ vcat (map pPrint ls)

instance Pretty Log where
  pPrint (Log msgs) = vcat (map pPrint msgs)

pPrintIntMap :: Pretty v => IntMap v -> PP.Doc
pPrintIntMap = pPrintIntMap' pPrint

pPrintIntMap' :: (v -> PP.Doc) -> IntMap v -> PP.Doc
pPrintIntMap' pv m = PP.braces $
  vcat [ (PP.int k <> PP.colon) <+> pv v
       | (k, v) <- IMap.assocs m]


pPrintMap :: (Pretty v, Pretty k) => Map k v -> PP.Doc
pPrintMap m = PP.braces $
  vcat [ (pPrint k <> PP.colon) <+> pPrint v
       | (k, v) <- Map.assocs m]


data FunDefs = FunDefs {
  _byType       :: Map (Ident, [Type], Type) MethodCode
  , _byCodeId   :: IntMap MethodCode
  , _flowGraph  :: IntMap (IntMap Edge)
  , _joinPoints :: IntMap IntSet
  }
makeLenses ''FunDefs

data TracingMode =
  WholeLoop
  | SingleTrace
  deriving (Show, Eq, Ord)


data Options = Opts {
  _tracingMode    :: TracingMode
  , _heatThresh   :: Double
  , _heatFunction :: Double -> Double
  , _coolFunction :: Double -> Double
  }
makeLenses ''Options

data ReadEnv = ReadEnv {
  _funDefs        :: FunDefs
  , _currentCode  :: MethodCode
  , _options      :: Options
  , _currentDepth :: Int
  }
makeLenses ''ReadEnv

data FinalState = FinalState {
  _stack           :: [Value]
  , _locals        :: IntMap Value
  , _log           :: Log
  , _statistics    :: Statistics
  , _compiledLoops :: Map LineId CompiledCode
  , _readEnv       :: ReadEnv
  , _heatMap       :: Map LineId Double
  }

instance Pretty FinalState where
  pPrint FinalState{..} =
    vsep
    [ text "Final State"
    , text "Flow Graph" <+> pPrintIntMap' pPrintIntMap (_readEnv ^. funDefs . flowGraph)
    , text "Join Points" <+> pPrintIntMap' (text . show) (_readEnv ^. funDefs . joinPoints)
    , text "Heat Map" <+> pPrintMap _heatMap
    , text "Log:" <+> pPrint (compressLog _log)
    , text "Compiled Loops:" <+> pPrintMap _compiledLoops
    , text "Stack:" <+> pPrintList PP.prettyNormal _stack
    , text "Locals:" <+> pPrintIntMap _locals
    , text "Statistics:" <+> pPrint _statistics
    ]


data RunError =
  Unexpected
  | Error String
  | LabelMiss Label
  deriving (Show, Eq)

instance Pretty RunError where
  pPrint e = case e of
    Unexpected    -> text "unexpected error"
    Error msg     -> text "generic error, message: " <> text msg
    LabelMiss lbl -> text "could not find label: " <> text (show lbl)

emptyTracing :: Tracing
emptyTracing = Tracing {
  _heatMap = mempty
  , _trace = mempty
  , _active = True
  }

emptyStatistics :: Statistics
emptyStatistics = Stats {
  _count = mempty
  , _scompiledMethods = mempty
  , _ffiCalls = 0
  , _compiledLinesCount = 0
  }

iniState :: IState p
iniState = IState {
  _stack = []
  , _locals = mempty
  , _tracing = emptyTracing
  , _stats = emptyStatistics
  , _compiledLoops = mempty
  , _compiledMethods = mempty
  }

defaultOptions :: Options
defaultOptions = Opts {
  _tracingMode = SingleTrace
  , _heatThresh = 100
  , _heatFunction = (+1)
  , _coolFunction = \h -> max nullHeat h -- (h - 0.01)
  }

compiledMethod :: CodeId -> Lens' (IState s) (Maybe CompiledMethod)
compiledMethod cid = compiledMethods . at (cid ^. codeIdInt)

methodByType :: (Ident, [Type], Type) -> Lens' ReadEnv (Maybe MethodCode)
methodByType sig = funDefs . byType . at sig

putPretty :: Pretty p => p -> IO ()
putPretty = putStrLn . PP.render . pPrint

runIO :: Program -> IO ()
runIO = either putPretty putPretty . runPure

lineHeat :: LineId -> Lens' (IState s) Double
lineHeat lid = tracing . heatMap . at lid . non nullHeat

runPure :: Program -> Either RunError FinalState
runPure p =
  case mainCode progFunDefs of
    Nothing -> Left (Error "main not found")
    Just main ->
      runST $ do
      let readenv = ReadEnv {
            _funDefs = progFunDefs
            , _currentCode = main
            , _options = defaultOptions
            , _currentDepth = 0
            }
      e <- runExceptT (runReaderT (aux <$> runStateT (runWriterT interpretCode) iniState) readenv)
      case e of
        Left err          -> return (Left err)
        Right (istate, l) -> Right <$> gatherResults readenv l istate
  where
    progFunDefs = scanDefs p
    aux ((_,w),s) = (s, w)

gatherResults :: PrimMonad m => ReadEnv -> Log -> IState (PrimState m) -> m FinalState
gatherResults renv l IState{..} = do
  finalStack <- mapM freezeValue _stack
  finalLocals <- mapM freezeValue _locals
  return FinalState {
    _stack = finalStack
    , _locals = finalLocals
    , _log = l
    , _statistics = _stats
    , _compiledLoops = _compiledLoops
    , _readEnv = renv
    , _heatMap = _tracing ^. heatMap
    }

newIntArrayRef :: PrimMonad m => Int -> m (MValue (PrimState m))
newIntArrayRef size = MArrayRef Int size <$> MVec.new size

freezeValue :: PrimMonad m => MValue (PrimState m) -> m Value
freezeValue val = case val of
  MInt i          -> return (VInt i)
  MArrayRef t s v -> ArrayRef t s <$> Vec.freeze v

edgeOutLabels :: Edge -> [Label]
edgeOutLabels e = case e of
  Seq n      -> n ^.. _Just
  Branch l n -> l : (n ^.. _Just)
  Invoke _ n -> n ^.. _Just

edgeBranchLabels :: Edge -> [Label]
edgeBranchLabels e = case e of
  Seq n      -> []
  Branch l n -> l : (n ^.. _Just)
  Invoke _ n -> []



scanDefs :: Program -> FunDefs
scanDefs (Prog _ _ funs) = fDefs
  where
    fDefs = FunDefs {
      _byType = Map.fromList [ ((iden, args, ret), mkMethodCode codeId code)
                             | (codeId, Method ret iden args code) <- enumFuns ]
      , _byCodeId = IMap.fromList [ (mthd ^. codeIden ^. codeIdInt, mthd)
                                  | mthd <- Map.elems (fDefs ^. byType) ]
      , _flowGraph =
        IMap.fromList [ (codeId, mkMethodGraph (mthd ^. code))
                      | (codeId, mthd) <- IMap.assocs (fDefs ^. byCodeId) ]
      , _joinPoints = IMap.fromList [ (codeId, mkJoinPoints flow)
                                    | (codeId, flow) <- IMap.assocs (fDefs ^. flowGraph) ]
      }
    enumFuns = zip [0..] funs
    -- A join point is a line that has at least two incoming edges in the
    -- flowgraph. The first line in a method is also a join point.
    mkJoinPoints :: IntMap Edge -> IntSet
    mkJoinPoints m = ISet.fromList (filter isJoinPoint lbls)
      where
        entryLabel = fst . head . IMap.toAscList $ m
        (lbls, edges) = unzip (IMap.toAscList m)
        --TODO clean
        ecount = foldr (\k -> IMap.insertWith (+) k 1) mempty labelsInts
          where labelsInts = map (^. labelInt) (collectBranchLabels edges)
        isJoinPoint lbl = lbl == entryLabel || (0 < (fromMaybe 0 (IMap.lookup lbl ecount)))
        collectLabels :: [Edge] -> [Label]
        collectLabels = concatMap edgeOutLabels
        collectBranchLabels :: [Edge] -> [Label]
        collectBranchLabels = concatMap edgeBranchLabels
    mkMethodGraph :: IntMap (NonEmpty Line) -> IntMap Edge
    mkMethodGraph = fmap mkEdge
      where
        mkEdge (ln :| ls) = case ln ^. lineInstr of
          Invokestatic _ ann ->
              case runExcept (lookupInvokeAnn (fDefs ^. byType) ann) of
                Left e     -> error (show e)
                Right mthd -> Invoke (mthd ^. codeIden) nextLabel
          Goto l -> Seq (Just l)
          i ->
           case i ^. jumpLabel of
              Nothing  -> Seq nextLabel
              Just lbl -> Branch lbl nextLabel
          where
            nextLabel = ls ^? each . lineLabel


mainCode :: FunDefs -> Maybe MethodCode
mainCode FunDefs {..} = Map.lookup (Ident "main", [], Void) _byType

jumpLabel :: SimpleGetter Instruction (Maybe Label)
jumpLabel = to aux
  where
    aux i =  case i of
      Goto l     -> Just l
      Ifge l     -> Just l
      Ifgt l     -> Just l
      Iflt l     -> Just l
      Ifle l     -> Just l
      Ifne l     -> Just l
      Ifeq l     -> Just l
      Ificmpge l -> Just l
      Ificmpgt l -> Just l
      Ificmple l -> Just l
      Ificmplt l -> Just l
      Ificmpne l -> Just l
      Ificmpeq l -> Just l
      _          -> Nothing

mkMethodCode :: CodeId -> [Line] -> MethodCode
mkMethodCode codeId = go mempty . sortOn (^. lineLabel)
  where go ac [] = MethodCode {
          _codeIden = codeId
          , _code = ac
        }
        go ac (ln : ls) =
          go (IMap.insert (ln ^. lineLabel ^. labelInt) (ln :| ls) ac) ls

-- Interpreter

interpretCode :: (PrimMonad m, MonadReader ReadEnv m, MonadError RunError m
                 , MonadState (IState (PrimState m)) m
                 , MonadWriter Log m)
              => m ()
interpretCode = do
  startFun <- view currentCode
  go (entryPoint startFun)

entryPoint :: MethodCode -> [Line]
entryPoint = maybe [] (L1.toList . fst) . IMap.minView . _code

canHeat :: MonadReader ReadEnv m => LineId -> m Bool
canHeat lid = (ISet.member lbl) . (IMap.! cid) <$> view (funDefs . joinPoints)
  where (cid, lbl) = lid ^. lineIdInts

viewLineId :: MonadReader ReadEnv m => Label -> m LineId
viewLineId lbl = LineId . (,lbl) <$> view (currentCode . codeIden)

internalLabel :: Label
internalLabel = Label (-1)

findLine :: (MonadReader ReadEnv m, MonadError RunError m) => LineId -> m Line
findLine (LineId (codeId, lbl))  = do
  mthd <- fromMaybeM err (IMap.lookup (codeId ^. codeIdInt) <$> view (funDefs . byCodeId))
  L1.head <$> lookupLabel lbl (mthd ^. code)
  where
    err = throwError (Error "CodeIden not found")

cool :: (MonadReader ReadEnv m, MonadState (IState (PrimState m)) m) => Label -> m ()
cool insulated = do
  lnId <- viewLineId insulated
  coolInstr <- view (options . coolFunction)
  let isInsulated = (== lnId)
  modifying (tracing . heatMap) (Map.mapWithKey (\k -> if isInsulated k
                                                       then id else coolInstr))

nullHeat :: Double
nullHeat = 0

-- | returns True if the Label is hot enough
heatUp :: (MonadReader ReadEnv m, MonadState (IState (PrimState m)) m)
  => Label -> m Bool
heatUp lbl = do
  lnId <- viewLineId lbl
  heatUpInstr <- view (options . heatFunction)
  modifying (tracing . heatMap) (Map.insertWith (\_ old -> heatUpInstr old) lnId nullHeat)
  temp <- Map.findWithDefault 0 lnId <$> use (tracing . heatMap)
  (temp >) <$> view (options . heatThresh)

updateHeat :: (MonadReader ReadEnv m, MonadState (IState (PrimState m)) m)
  => Label -> m Bool
updateHeat lbl = cool lbl >> heatUp lbl

startTrace :: MonadState (IState (PrimState m)) m => m ()
startTrace = assign (tracing . trace) (Just mempty)

hasTrace :: MonadState (IState (PrimState m)) m => m Bool
hasTrace = isJust <$> use (tracing . trace)

addToTrace :: (MonadReader ReadEnv m, MonadState (IState (PrimState m)) m
              , MonadError RunError m)
  => Label -> m (Maybe [TraceItem])
addToTrace lbl = do
  lid@(LineId (cid,_)) <- viewLineId lbl
  depth <- view currentDepth
  let mkLnId = LineId . (cid,)
      mkItem lid = TraceItem {
        _itemLineId = lid
        , _callDepth = depth
        }
      thisItem = mkItem lid
  traceMode <- view (options . tracingMode)
  ifJustM (use (tracing . trace)) (return Nothing) $ \utrce -> do
    let pushToTrace = assign (tracing . trace) (Just (mkItem lid : utrce)) >> return Nothing
    ret <- case traceMode of
      WholeLoop -> error "not supported"
  -- let fromThisMethod = (==cid) . (^. lineIdCodeId)
  --           ftrce = filter fromThisMethod utrce
  --       if elem lid ftrce then do
  --         end <- findLoopEnd ftrce
  --         start <- findLoopStart lid end
  --         Just . takeWhile (<= end) . map (mkLnId . (^. lineLabel)) <$> atLabel (start ^. lineIdLabel)
  --         else pushToTrace

      SingleTrace ->
        case findIndex (==thisItem) utrce of
          Just ix -> do
            let path = reverse (take (ix + 1) utrce)
            return (Just path)
          Nothing -> pushToTrace
    return ret

findLoopEnd :: (MonadReader ReadEnv m, MonadState (IState (PrimState m)) m
              , MonadError RunError m) => [TraceItem] -> m TraceItem
findLoopEnd trce = return (maximumBy (compare `on` (^. itemLineId)) trce)

findLoopStart :: (MonadReader ReadEnv m, MonadState (IState (PrimState m)) m
              , MonadError RunError m) => LineId -> LineId -> m LineId
findLoopStart (LineId (cid, lbl)) end = do
  loopFragment <- takeWhile ((<= end) . mkLnId . (^. lineLabel)) <$> atLabel lbl
  let minJumpLabel = minimum (mapMaybe (^. (lineInstr . jumpLabel)) loopFragment)
  return (LineId (cid, minJumpLabel))
  where
    mkLnId = LineId . (cid,)

tell1 :: MonadWriter Log m => LogMsg -> m ()
tell1 = tell . Log . pure

logExit :: MonadWriter Log m => Line -> m ()
logExit = tell1 . LogExitLoop

logEnter :: (MonadState (IState (PrimState m)) m, MonadWriter Log m) => Line -> m ()
logEnter l = do
  modifying (stats . ffiCalls) (+1)
  tell1 (LogEnterLoop l)

logJump :: MonadWriter Log m => Label -> m ()
logJump lbl = do
  tell1 (LogJump lbl)

logCompiled :: MonadWriter Log m => CompiledCode -> m ()
logCompiled = tell1 . LogCompiled

logCompile :: (MonadState (IState (PrimState m)) m, MonadReader ReadEnv m
              , MonadWriter Log m) => Label -> [Line] -> m ()
logCompile lbl ls = do
  lid <- viewLineId lbl
  modifying (stats . compiledLinesCount) (+length ls)
  tell1 (LogCompile lid ls)

logCompileMethod :: (MonadState (IState (PrimState m)) m, MonadWriter Log m)
  => MethodCode -> m ()
logCompileMethod mc = do
  modifying (stats . compiledLinesCount) (+(length (entryPoint mc)))
  tell1 (LogCompileMethod mc)

logDebug :: MonadWriter Log m => String -> m ()
logDebug = tell1 . LogDebug

runCompiled
  :: (PrimMonad m, MonadReader ReadEnv m, MonadError RunError m
      , MonadState (IState (PrimState m)) m
      , MonadWriter Log m)
  => Label -> CompiledCode -> m ()
runCompiled lbl compiled = do
  setNativeMode
  currId <- view (currentCode . codeIden)
  renv <- ask
  let mc = mkMethodCode currId codeLines
      isolatedRE = set currentCode mc renv
  entry <- L1.toList <$> lookupLabel lbl (mc ^. code)
  runReaderT (go entry) isolatedRE
    where codeLines = compiled ^. compiledLines

continueCompiled
  :: (PrimMonad m, MonadReader ReadEnv m, MonadError RunError m
      , MonadState (IState (PrimState m)) m
      , MonadWriter Log m)
  => Label -> CompiledCode -> m ()
continueCompiled lbl c = do
  logJump lbl
  runCompiled lbl c

igoto :: (PrimMonad m, MonadWriter Log m, MonadReader ReadEnv m
        , MonadState (IState (PrimState m)) m
        , MonadError RunError m) => Label -> m ()
igoto lbl =
  do
    cl <- viewLineId lbl >>= lookupCompiledLoop
    case cl of
      Nothing -> error ("internal jump pointing to nonexisting compiled loop " ++ show lbl)
      Just cl -> continueCompiled lbl cl

iinterpret :: (PrimMonad m, MonadWriter Log m, MonadReader ReadEnv m
        , MonadState (IState (PrimState m)) m
        , MonadError RunError m) => Label -> m ()
iinterpret lbl = do
  viewLineId lbl >>= findLine >>= logExit
  setInterpreterMode
  restored <- restoreCurrentMethod
  runReaderT (atLabel lbl >>= go) restored


restoreCurrentMethod :: MonadReader ReadEnv m => m ReadEnv
restoreCurrentMethod = do
  renv <- ask
  let cid = renv ^. currentCode . codeIden . codeIdInt
      mc = fromJust . IMap.lookup cid $ (renv ^. funDefs . byCodeId)
  return (set currentCode mc renv)

toggleTracing :: (MonadState (IState (PrimState m)) m) => Bool -> m ()
toggleTracing = assign (tracing . active)

compileCache :: (MonadWriter Log m, MonadReader ReadEnv m
           , MonadError RunError m, MonadState (IState (PrimState m)) m)
  => [Line] -> CodeId -> m CompiledCode
compileCache ls cid = do
  let toLid = LineId . (cid,) . (^. lineLabel)
      lids = map toLid ls
      headlids = head lids
      entrylbl = headlids ^. lineIdLabel
      compileCode = fmap (CompiledCode entrylbl . sortOn (^. lineLabel)) .
        (changeExitPoints cid)
  m <- use (compiledLoops . at headlids)
  case m of
    Nothing -> do
      forM_ lids (\lid -> assign (stats . lineWasCompiled lid) True)
      compiledCode <- compileCode ls
      logCompiled compiledCode
      assign (compiledLoops . at headlids) (Just compiledCode)
      updateCompiledExits headlids
      -- methodCalls ls >>= mapM_ compileMethodCache
      return compiledCode
    Just c -> return c
  where
    sortls = sortOn getlbl ls
    getlbl = (^. lineLabel)

sortOnLabel :: [Line] -> [Line]
sortOnLabel = sortOn (^. lineLabel . labelInt)

nubOnLabel :: [Line] -> [Line]
nubOnLabel = nubOn (^. lineLabel . labelInt)

-- TODO: refactor to avoid code repetition
changeExitPoints :: (MonadState (IState (PrimState m)) m
                     , MonadReader ReadEnv m, MonadError RunError m)
  => CodeId -> [Line] -> m [Line]
changeExitPoints cid ls' =
  do
    compilids <- Map.keysSet <$> use compiledLoops
    extralines <- nubOnLabel <$> (mapMaybeM (aux compilids) ls)
    return $
      -- map substInvoke
      (sortOnLabel (ls ++ extralines))
  where
    substInvoke l = case l of
      Line lbl Invokestatic{} -> Line lbl (I_Interp lbl)
      _ -> l
    ls = sortOnLabel ls'
    lblset = ISet.fromAscList (map (^. lineLabel . labelInt) ls)
    lblmember = (`ISet.member` lblset) . (^. labelInt)
    mklid = LineId . (cid, )
    aux compilids a = do
      r <- findNextGraphLabel cid a
      return $
        case (r, a ^. lineInstr . jumpLabel) of
          (Just nextlbl, Just jmplbl)
            | memnext && memjmp -> Nothing
            | not memnext -> Just . Line nextlbl $ if
              | Set.member nextlid compilids -> I_GotoCompiled nextlbl
              | otherwise -> I_Interp nextlbl
            | not memjmp -> Just . Line jmplbl $ if
                | Set.member nextlid compilids -> I_GotoCompiled jmplbl
                | otherwise -> I_Interp jmplbl
            | otherwise -> error "at least one must be in the trace"
            where
              nextlid = mklid nextlbl
              jmplid = mklid jmplbl
              memnext = lblmember nextlbl
              memjmp = lblmember jmplbl
          (Just nextlbl, Nothing)
            | memnext -> Nothing
            | otherwise -> Just . Line nextlbl $ if
              | Set.member nextlid compilids -> I_GotoCompiled nextlbl
              | otherwise -> I_Interp nextlbl
              where
                memnext = lblmember nextlbl
                nextlid = mklid nextlbl
          (Nothing, _) -> Nothing
            -- error ("the last line before halting should never be part of a loop " ++ show (r, a))

updateCompiledExits :: (MonadState (IState (PrimState m)) m)
  => LineId -> m ()
updateCompiledExits lid =
  modifying compiledLoops (fmap aux)
  where
    aux :: CompiledCode -> CompiledCode
    aux = over compiledLines (map update)
    update ln
      | I_Interp lbl <- ln ^. lineInstr
      , lbl == lid ^. lineIdLabel = set lineInstr (I_GotoCompiled lbl) ln
      | otherwise = ln

findEdge :: (MonadReader ReadEnv m
              , MonadError RunError m)
              => LineId -> m Edge
findEdge lid = fromMaybeM err ((IMap.lookup cid >=> IMap.lookup lbl) <$> view (funDefs . flowGraph))
  where err = throwError (Error ("edge not found: LineId = " ++ show lid))
        (cid, lbl) = lid ^. lineIdInts

findNextGraphLabel :: (MonadReader ReadEnv m
                 , MonadError RunError m)
  => CodeId -> Line -> m (Maybe Label)
findNextGraphLabel cid l = case l of
  Line _ (I_GotoCompiled lbl) -> return (Just lbl)
  _                           -> (^. edgeNextLabel) <$> findEdge lid
  where lbl = l ^. lineLabel
        mklid = LineId . (cid,)
        lid = mklid (l ^. lineLabel)

compileMethodCache
  :: (MonadWriter Log m, MonadReader ReadEnv m
     , MonadError RunError m
     , MonadState (IState (PrimState m)) m)
  => MethodCode -> m CompiledMethod
compileMethodCache m = do
  let cid = m ^. codeIden
  c <- use (compiledMethod cid)
  case c of
    Just c -> return c
    Nothing -> do
      assign (compiledMethod cid) (Just cmethod)
      modifying (stats . scompiledMethods) (ISet.insert (cid ^. codeIdInt))
      logCompileMethod m
      void (compileCache (entryPoint m) cid)
      return cmethod
  where cmethod = CMethod m

methodCalls :: (MonadError RunError m, MonadReader ReadEnv m)
  => [Line] -> m [MethodCode]
methodCalls = fmap (nubOn (^. codeIden)) . mapMaybeM f . map (^. lineInstr)
  where
    f ln = case ln of
      Invokestatic _ ann ->
        Just <$> (parseInvokeAnn ann >>= findMethodByType)
      _ -> return Nothing


interpreterMode :: MonadState (IState (PrimState m)) m => m Bool
interpreterMode = use (tracing . active)

resetTracing :: MonadState (IState (PrimState m)) m => m ()
resetTracing = assign tracing emptyTracing

setInterpreterMode :: MonadState (IState (PrimState m)) m => m ()
setInterpreterMode = assign (tracing . active) True

setNativeMode :: MonadState (IState (PrimState m)) m => m ()
setNativeMode = assign (tracing . active) False

emptyTrace :: MonadState (IState (PrimState m)) m => m ()
emptyTrace = assign (tracing . trace) Nothing

labelIsInternal :: Label -> Bool
labelIsInternal lbl = lbl ^. labelInt < 0

incrCount :: (MonadState (IState (PrimState m)) m, MonadReader ReadEnv m)
  => Label -> m ()
incrCount lbl | labelIsInternal lbl = return ()
incrCount lbl = do
  t <- interpreterMode
  lid <- viewLineId lbl
  let ctr = if t then countInterpreted else countCompiled
  modifying (stats . ctr lid) (+1)

pShow :: Pretty a => a -> String
pShow = PP.render . pPrint

lookupCompiledLoop :: MonadState (IState (PrimState m)) m
  => LineId -> m (Maybe CompiledCode)
lookupCompiledLoop lid = Map.lookup lid <$> use compiledLoops

resetHeat :: (MonadState (IState s) m) => [LineId] -> m ()
resetHeat = mapM_ (\lid -> assign (lineHeat lid) nullHeat)

go :: (PrimMonad m, MonadReader ReadEnv m, MonadError RunError m
      , MonadState (IState (PrimState m)) m
      , MonadWriter Log m)
   => [Line] -> m ()
go [] = return ()
go (l@(Line lbl i):is) = do
  t <- interpreterMode
  if t then do
    lid <- viewLineId lbl
    res <- lookupCompiledLoop lid
    case res of
      Just c -> runThisCompiled c
      Nothing -> do
        whenM (canHeat lid) $
          whenM ((not <$> hasTrace) &&^ updateHeat lbl) startTrace
        mayloop <- addToTrace lbl
        case mayloop of
          Just loopIds -> do
            loop <- mapM (findLine . (^. itemLineId)) loopIds
            logCompile lbl loop
            cid <- view (currentCode . codeIden)
            compiledLoop <- compileCache loop cid
            emptyTrace
            resetHeat (map (^. itemLineId) loopIds)
            runThisCompiled compiledLoop
          Nothing -> gorun
    else gorun
  where
    runThisCompiled c = do
      lid <- viewLineId lbl
      findLine lid >>= logEnter
      runCompiled lbl c
    cont = go is
    gorun = do
      incrCount lbl
      interp
    interp =
      case i of
        Iconst_0            -> iconst 0 >> cont
        Iconst_1            -> iconst 1 >> cont
        Iconst_2            -> iconst 2 >> cont
        Iconst_3            -> iconst 3 >> cont
        Iconst_4            -> iconst 4 >> cont
        Iconst_5            -> iconst 5 >> cont
        Iconst_m1           -> iconst (-1) >> cont
        Bipush k            -> bipush (fromInteger k) >> cont
        Sipush k            -> pushIntVal (fromInteger k) >> cont
        Iload_0             -> iload 0 >> cont
        Iload_1             -> iload 1 >> cont
        Iload_2             -> iload 2 >> cont
        Iload_3             -> iload 3 >> cont
        Iload j             -> iload j >> cont
        Ldc _ (LdcAnnInt i) -> iconst (fromInteger i) >> cont
        Istore_0            -> istore 0 >> cont
        Istore_1            -> istore 1 >> cont
        Istore_2            -> istore 2 >> cont
        Istore_3            -> istore 3 >> cont
        Istore j            -> istore j >> cont
        Astore_0            -> astore 0 >> cont
        Astore_1            -> astore 1 >> cont
        Astore_2            -> astore 2 >> cont
        Astore_3            -> astore 3 >> cont
        Astore j            -> astore j >> cont
        Iadd                -> iadd >> cont
        Isub                -> isub >> cont
        Imul                -> imul >> cont
        Irem                -> irem >> cont
        Iinc j k            -> iinc j (fromInteger (fromSInteger k)) >> cont
        Ifge l              -> ifBranch testge l cont
        Ifgt l              -> ifBranch testgt l cont
        Ifle l              -> ifBranch testle l cont
        Iflt l              -> ifBranch testlt l cont
        Ifne l              -> ifBranch testne l cont
        Ifeq l              -> ifBranch testeq l cont
        Ificmpge l          -> ifBranch testcmpge l cont
        Ificmpgt l          -> ifBranch testcmpgt l cont
        Ificmple l          -> ifBranch testcmple l cont
        Ificmplt l          -> ifBranch testcmplt l cont
        Ificmpeq l          -> ifBranch testcmpeq l cont
        Ificmpne l          -> ifBranch testcmpne l cont
        I_GotoCompiled l    -> igoto l
        I_Interp l          -> iinterpret l
        Newarray t          -> newarray t >> cont
        Arraylength         -> arraylength >> cont
        Iastore             -> iastore >> cont
        Iaload              -> iaload >> cont
        Aload_0             -> aload 0 >> cont
        Aload_1             -> aload 1 >> cont
        Aload_2             -> aload 2 >> cont
        Aload_3             -> aload 3 >> cont
        Invokestatic _ ann  -> invokestatic ann >> cont
        Dup                 -> dup >> cont
        Dup2                -> dup2 >> cont
        Pop                 -> popVal >> cont
        Goto l              -> atLabel l >>= go
        Nop                 -> cont
        Ireturn             -> return ()
        Return              -> return ()

ifBranch :: (MonadReader ReadEnv m, MonadError RunError m, PrimMonad m
            , MonadState (IState (PrimState m)) m
            , MonadWriter Log m)
  => m Bool -> Label -> m () -> m ()
ifBranch test label = ifM test (atLabel label >>= go)

pushVal :: MonadState (IState (PrimState m)) m => MValue (PrimState m) -> m ()
pushVal val = modifying stack (val:)

pushIntVal :: MonadState (IState (PrimState m)) m => Int -> m ()
pushIntVal = pushVal . MInt

unexpected :: MonadError RunError m => m a
unexpected = throwError Unexpected

intVal :: MonadError RunError m => MValue p -> m Int
intVal (MInt i) = return i
intVal _        = throwError (Error "expected Int")

mArrayRefVal :: MonadError RunError m
  => MValue s -> m (MVector s Int)
mArrayRefVal (MArrayRef _ _ v) = return v
mArrayRefVal _                 = throwError (Error "expected array ref")


popVal :: (MonadState (IState (PrimState m)) m, MonadError RunError m)
  => m (MValue (PrimState m))
popVal = do
  s <- use stack
  case s of
    []    -> throwError (Error "empty stack")
    (v:_) -> modifying stack tail >> return v

popIntVal :: (MonadState (IState (PrimState m)) m, MonadError RunError m) => m Int
popIntVal = popVal >>= intVal

popMArrayRefVal :: (MonadState (IState (PrimState m)) m, MonadError RunError m)
  => m (MVector (PrimState m) Int)
popMArrayRefVal = popVal >>= mArrayRefVal

dup :: (MonadState (IState (PrimState m)) m, MonadError RunError m) => m ()
dup = do
  v <- popVal
  pushVal v >> pushVal v

dup2 :: (MonadState (IState (PrimState m)) m, MonadError RunError m) => m ()
dup2 = do
  a <- popVal
  b <- popVal
  replicateM_ 2 (pushVal b >> pushVal a)

-- Instructions

ibinop :: (MonadError RunError m, MonadState (IState (PrimState m)) m)
  => (Int -> Int -> Int) -> m ()
ibinop op = do
  b <- popIntVal
  a <- popIntVal
  pushIntVal (op a b)

iadd :: (MonadError RunError m, MonadState (IState (PrimState m)) m) => m ()
iadd = ibinop (+)

imul :: (MonadError RunError m, MonadState (IState (PrimState m)) m) => m ()
imul = ibinop (*)

isub :: (MonadError RunError m, MonadState (IState (PrimState m)) m) => m ()
isub = ibinop (-)

irem :: (MonadError RunError m, MonadState (IState (PrimState m)) m) => m ()
irem = ibinop mod

bipush :: MonadState (IState (PrimState m)) m => Int -> m ()
bipush = pushIntVal

iconst :: MonadState (IState (PrimState m)) m => Int -> m ()
iconst = pushIntVal

istore :: (MonadError RunError m, MonadState (IState (PrimState m)) m) => LocalIx -> m ()
istore = storeVal

astore :: (MonadError RunError m, MonadState (IState (PrimState m)) m) => LocalIx -> m ()
astore = storeVal

iload :: (MonadError RunError m, MonadState (IState (PrimState m)) m) => LocalIx -> m ()
iload = loadVal

aload :: (MonadError RunError m, MonadState (IState (PrimState m)) m) => LocalIx -> m ()
aload = loadVal

loadVal :: (MonadError RunError m, MonadState (IState (PrimState m)) m) => LocalIx -> m ()
loadVal ((^. localIxInt) -> j) =
  fromMaybeM (errorCtx "loadVal") (IMap.lookup j <$> use locals) >>= pushVal

errorCtx :: (MonadError RunError m, MonadState (IState (PrimState m)) m)
  => String -> m a
errorCtx msg = do
  ctx <- get
  throwError (Error (msg ++ ":\n" ++ show ctx))

storeVal :: (MonadError RunError m, MonadState (IState (PrimState m)) m) => LocalIx -> m ()
storeVal ((^. localIxInt) -> j) = do
  v <- popVal
  modifying locals (IMap.insert j v)

atLabel1 :: (MonadReader ReadEnv m, MonadError RunError m) => Label -> m (NonEmpty Line)
atLabel1 lbl = view (currentCode . code) >>= lookupLabel lbl

atLabel :: (MonadReader ReadEnv m, MonadError RunError m) => Label -> m [Line]
atLabel = fmap L1.toList . atLabel1

lookupLabel :: MonadError RunError m => Label -> IntMap (NonEmpty Line) -> m (NonEmpty Line)
lookupLabel lbl mthd = maybe err return (IMap.lookup (lbl ^. labelInt) mthd)
  where err = throwError (LabelMiss lbl)

testnum :: (MonadError RunError m, MonadState (IState (PrimState m)) m)
  => (forall n. (Ord n, Num n) => n -> Bool) -> m Bool
testnum test = do
  v <- popVal
  case v of
    MInt i -> return (test i)
    _      -> unexpected

testge :: (MonadError RunError m, MonadState (IState (PrimState m)) m) => m Bool
testge = testnum (>= 0)

testgt :: (MonadError RunError m, MonadState (IState (PrimState m)) m) => m Bool
testgt = testnum (> 0)

testle :: (MonadError RunError m, MonadState (IState (PrimState m)) m) => m Bool
testle = testnum (<= 0)

testlt :: (MonadError RunError m, MonadState (IState (PrimState m)) m) => m Bool
testlt = testnum (< 0)

testeq :: (MonadError RunError m, MonadState (IState (PrimState m)) m) => m Bool
testeq = testnum (== 0)

testne :: (MonadError RunError m, MonadState (IState (PrimState m)) m) => m Bool
testne = testnum (/= 0)

testcmpge :: (MonadError RunError m, MonadState (IState (PrimState m)) m) => m Bool
testcmpge = testnums (>=)

testcmpgt :: (MonadError RunError m, MonadState (IState (PrimState m)) m) => m Bool
testcmpgt = testnums (>)

testcmpeq :: (MonadError RunError m, MonadState (IState (PrimState m)) m) => m Bool
testcmpeq = testnums (==)

testcmpne :: (MonadError RunError m, MonadState (IState (PrimState m)) m) => m Bool
testcmpne = testnums (/=)

testcmple :: (MonadError RunError m, MonadState (IState (PrimState m)) m) => m Bool
testcmple = testnums (<=)

testcmplt :: (MonadError RunError m, MonadState (IState (PrimState m)) m) => m Bool
testcmplt = testnums (<)

testnums :: (MonadError RunError m, MonadState (IState (PrimState m)) m)
 => (forall n. (Ord n, Num n) => n -> n -> Bool) -> m Bool
testnums test = do
  vb <- popVal
  va <- popVal
  case (va, vb) of
    (MInt a, MInt b) -> return (test a b)
    _                -> unexpected

iinc :: (MonadError RunError m, MonadState (IState (PrimState m)) m)
  => LocalIx -> Int -> m ()
iinc ((^. localIxInt) -> j) k = do
  val <- fromMaybeM unexpected (IMap.lookup j <$> use locals)
  case val of
    MInt i -> modifying locals (IMap.insert j (MInt (i + k)))
    _      -> throwError (Error "expected int")

newarray :: (PrimMonad m, MonadError RunError m, MonadState (IState (PrimState m)) m)
  => Type -> m ()
newarray t = case t of
  Int -> do
    size <- popIntVal
    newIntArrayRef size >>= pushVal
  _ -> unexpected

arraylength :: (MonadError RunError m, MonadState (IState (PrimState m)) m)
  => m ()
arraylength = do
  mvec <- popMArrayRefVal
  pushIntVal (MVec.length mvec)

iaload :: (PrimMonad m, MonadError RunError m, MonadState (IState (PrimState m)) m)
  => m ()
iaload = do
  i <- popIntVal
  mvec <- popMArrayRefVal
  MVec.read mvec i >>= pushIntVal

iastore :: (PrimMonad m, MonadError RunError m, MonadState (IState (PrimState m)) m)
  => m ()
iastore = do
  val <- popIntVal
  i <- popIntVal
  vec <- popMArrayRefVal
  MVec.write vec i val

invokestatic :: (PrimMonad m, MonadError RunError m
                , MonadState (IState (PrimState m)) m, MonadReader ReadEnv m
                , MonadWriter Log m)
             => InvokestaticAnn -> m ()
invokestatic ann@(InvokestaticAnnMethod _name argsspec retspec) = do
  backloc <- use locals
  sig@(_, args, _) <- parseInvokeAnn ann
  interpMode <- interpreterMode
  invokedCode <- if
    | interpMode -> findMethodByType sig
    | otherwise -> findCompiledMethodByType sig
  assign locals mempty
  loadArgs args
  methodenv <-
    over currentDepth (+1)
    . set currentCode invokedCode
    <$> ask
  runReaderT interpretCode methodenv
  assign locals backloc

lookupInvokeAnn :: MonadError RunError m
  => Map (Ident, [Type], Type) MethodCode -> InvokestaticAnn -> m MethodCode
lookupInvokeAnn m (InvokestaticAnnMethod name argspec retspec) = do
  args <- parseArgsTypes argspec
  ret <- parseRetType retspec
  maybe err return (Map.lookup (name, args, ret) m)
  where
    err = throwError (Error "type signature not found")

findMethodByType
  :: (MonadError RunError m, MonadReader ReadEnv m) =>
     (Ident, [Type], Type) -> m MethodCode
findMethodByType sig =
  fromMaybeM err $ Map.lookup sig <$> view (funDefs . byType)
  where err = throwError $ Error "method not found"

findCompiledMethodByType
  :: (MonadState (IState (PrimState m)) m, MonadError RunError m, MonadReader ReadEnv m) =>
     (Ident, [Type], Type) -> m MethodCode
findCompiledMethodByType sig = do
  cid <- (^. codeIden) <$> findMethodByType sig
  cm <- use compiledMethods
  let err = throwError (Error ("compiled method not found" ++ show sig ++ "\n" ++ show cm))
  fromMaybeM err (fmap uncompile . IMap.lookup (cid ^. codeIdInt) <$> use compiledMethods)
  where
    uncompile (CMethod c) = c


parseArgsTypes :: MonadError RunError m => ArgsSpec -> m [Type]
parseArgsTypes (ArgsSpec str) = go str
  where go ""       = return []
        go ('I':as) = (Int:) <$> go as
        go _        = throwError (Error "failed to parse arg types")

parseRetType :: MonadError RunError m => ArgsSpec -> m Type
parseRetType retspec = do
  l <- parseArgsTypes retspec
  case l of
    [r] -> return r
    _   -> throwError (Error "failed to parse ret type")

parseInvokeAnn :: MonadError RunError m => InvokestaticAnn -> m (Ident, [Type], Type)
parseInvokeAnn (InvokestaticAnnMethod name argspec retspec) = do
  args <- parseArgsTypes argspec
  ret <- parseRetType retspec
  return (name, args, ret)


loadArgs :: (MonadError RunError m, MonadState (IState (PrimState m)) m) => [Type] -> m ()
loadArgs args =
  forM_ (reverse (zip [0..] args)) $ \(LocalIx -> ix, tyarg) ->
    case tyarg of
      Int -> istore ix
      _   -> throwError (Error "wrong type argument")
