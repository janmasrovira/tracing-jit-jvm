module Extra where

import           Control.Monad.Except
import           Control.Monad.State
import           Data.Function
import           Data.List
import           Data.Map                       (Map)
import qualified Data.Map                       as Map
import           Data.Set                       (Set)
import qualified Data.Set                       as Set
import qualified Text.PrettyPrint.HughesPJClass as PP

-- | Inserts a new element. Also returns the replaced element, if any.
insertLookup :: Ord k => k -> a -> Map k a -> (Maybe a, Map k a)
insertLookup = Map.insertLookupWithKey (\_ a _ -> a)

insertOn :: Ord a => (b -> a) -> b -> [b] -> [b]
insertOn f = insertBy (compare `on` f)

-- | Analogous to 'either'.
except :: (e -> b) -> (a -> b) -> Except e a -> b
except f g = either f g . runExcept

-- | Recover form an exception.
fromExcept :: (e -> a) -> Except e a -> a
fromExcept f = either f id . runExcept

unsafe :: Show e => Except e a -> a
unsafe = either (error . show) id . runExcept

exceptCatch :: MonadError e' m => (e -> m a) -> Except e a -> m a
exceptCatch fe = except fe return

fromMaybeM :: Monad m => m a -> m (Maybe a) -> m a
fromMaybeM ma x = x >>= maybe ma return

-- | Discards state changes.
localState :: MonadState s m => m a -> m a
localState ma = do
  s <- get
  r <- ma
  put s
  return r

ifJustM :: Monad m => m (Maybe a) -> m b -> (a -> m b) -> m b
ifJustM mb mfalse mtrue = do
  r <- mb
  case r of
    Nothing -> mfalse
    Just x  -> mtrue x

setMaxElemView :: Set a -> Maybe a
setMaxElemView = fmap fst . Set.maxView

setMinElemView :: Set a -> Maybe a
setMinElemView = fmap fst . Set.minView

maybeMonoid :: Monoid a => Bool -> a -> a
maybeMonoid True  = id
maybeMonoid False = const mempty

vsep :: [PP.Doc] -> PP.Doc
vsep = PP.vcat . intersperse (PP.text "")
